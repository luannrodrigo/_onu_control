module.exports = function(application) {

	application.get('/', function(req, res){
		let connection = application.config.dbConnection();	//executa função de conexão com o banco
		let onuModel = application.app.models.onuModel; //tras a query que esta no arquivo model
		//
		onuModel.getOnu(connection, function(err, result){
			res.render("index", {onu: result});
		});

		// Rota do fomulario
		application.post('/index/save', function(req, res){
			var objOnu = req.body; 		//recuperando inf. do form
			//função que é recupeda da model
			onuModel.salvarOnu(objOnu, connection, function(err, result){
				res.render("/", {onu: result});
			});
		});
	});
};
