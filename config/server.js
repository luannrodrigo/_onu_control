// requerendo os middleware
var express = require('express')
var consign = require('consign')
var bodyParser = require('body-parser')
var expressValidator = require('express-validator')

var app = express()
//setando as engine de views
app.set('view engine', 'ejs')
//setando o diretório que as views se encontram
app.set('views', './app/views')

app.use(express.static('./app/public'))

//setando o body-parser para envio do form
app.use(bodyParser.urlencoded({extended: true}))
app.use(expressValidator())

/*Setando o diretorio rotas com o consign, elas vão carregar automaticamente*/
consign()
	.include('app/routes')
	.then('config/dbConnection.js')
	.then('app/models')
	.then('app/controllers')
	.into(app)

module.exports = app
